VERSION=$(git show -s --format="%H")
AUTHOR=$(git show -s --pretty=format:'%an')
MESSAGE=$(git show -s --pretty=format:'%s')

curl https://api.rollbar.com/api/1/deploy/ \
  -F access_token=428c2bfbb55f4dea85fee0c1ae64eb66 \
  -F environment=demo \
  -F revision="$VERSION" \
  -F local_username="$AUTHOR" \
  -F comment="$MESSAGE"
