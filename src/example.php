<?php

require_once __DIR__ . '/../vendor/autoload.php';

use \Rollbar\Rollbar;
use \Rollbar\Payload\Level;

Rollbar::init(
    [
        'access_token' => '428c2bfbb55f4dea85fee0c1ae64eb66',
        'environment' => 'demo',
        'code_version' => getCodeVersion(),
        'root' => dirname(__DIR__),
    ]
);

function getCodeVersion()
{
    return shell_exec('git rev-parse HEAD');
}

/**
 * Подозреваю,
 * что возможно
 * дело в комментариях
 * на русском
 */
function anotherFunctionWithError($someNumber)
{
    // Division by zero error will be thrown here
    return $someNumber / 0;
}


anotherFunctionWithError(333);
